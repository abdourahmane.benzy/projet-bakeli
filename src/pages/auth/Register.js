import React, { useState,useEffect } from "react";
import { auth,dbFirestore } from "../../firebase";
import { toast} from "react-toastify";
import { useSelector } from "react-redux";

 
const Register = ({history}) => {
  const [email, setEmail] = useState("");
  const [firstName, setFirstname] = useState('');
  const [lastName, setLastname] = useState('');
  const [password, setPassword] = useState('');
  const [signUp, setsign] = useState('');
  const [role, setRole] = useState('');
  const [btn, setBtn] = useState(false)
 
   const {user}=useSelector((state)=>({...state}));

    useEffect(() => {
      if(user){
        history.push("/")
      }
      if (email !=='' && firstName  !=='' && lastName  !=='' && password  !=='') {
          setBtn(true)
      }
    }, [user,password]);

    //^Register user
  const handleSubmit = async (e) => {
    e.preventDefault();

    //^After registration redirect in email
    const config = {
      url: process.env.REACT_APP_REGISTER_REDIRECT_URL,
      handleCodeInApp: true,
    };
    
    //^fiirebase to send email (user email,config)
    // await auth.sendSignInLinkToEmail(email, config);
    const signUp=auth.createUserWithEmailAndPassword(email,password).then(res =>{
       dbFirestore.doc(res.user.uid).set({email,firstName,lastName,role})
    });
    
    const {user} = signUp;

    // console.log(user)

    // const logFirestore= await dbFirestore.doc(user.uid).set({email});

    if (signUp !=='') {
      toast.success(
        `Email is sent to ${email}. Click the link to complete your registration.`
      );
      //^save user email in local storage
      window.localStorage.setItem("emailForRegistration", email);
  
      //^clear state
      setEmail("");
    }
   
  };

  const btnRegiste =btn? (<button type="submit" className="btn btn-raised">Register</button>):(<button type="submit" disabled className="btn btn-raised">Register</button>);

  const registerForm = () => (
    <form onSubmit={handleSubmit}>
      <input
        type="email"
        className="form-control"
        value={email}
        onChange={(e) => setEmail(e.target.value)}
        autoFocus
        placeholder='email'
      />
      <input
        type="text"
        className="form-control"
        value={firstName}
        onChange={(e) => setFirstname(e.target.value)}
        autoFocus
        placeholder='first name'
      />
      <input
        type="text"
        className="form-control"
        value={lastName}
        onChange={(e) => setLastname(e.target.value)}
        autoFocus
        placeholder='last name'
      />
      <input
        type="password"
        className="form-control"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
        autoFocus
        placeholder='password'
      />
      <select
        name="role"
        className="form-control"
        onChange={(e) => setRole(e.target.value)}
        autoFocus
      >
        <option value="">--Please choose an option--</option>
        <option value="apprenant">apprenant</option>
      </select>
     <br/>
     {btnRegiste}
    </form>
  );
 
  return (
    <div className="container p-5">
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <h4>Register</h4>
          {registerForm()}
        </div>
      </div>
    </div>
  );
};
 
export default Register;