import React from "react";
import { Link } from "react-router-dom";

const UserNav = () => (
  <nav>
    <ul className="nav flex-column">
      <li className="nav-item">
        <Link to="/" className="nav-link">
          Cours
        </Link>
      </li>
      <li className="nav-item">
        <Link to="/" className="nav-link">
          Calendrier
        </Link>
      </li>
      <li className="nav-item">
        <Link to="/user/password" className="nav-link">
        Changer   Password
        </Link>
      </li>
    </ul>
  </nav>
);

export default UserNav;
